FROM ubuntu:16.04 
MAINTAINER Pavel Terekhov 

RUN apt-get update 
RUN apt-get -y install default-jdk 
COPY *.jar ./
CMD java -jar *.jar


EXPOSE 8080
EXPOSE 9000
